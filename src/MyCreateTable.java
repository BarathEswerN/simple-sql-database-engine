package dubstep;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;

import net.sf.jsqlparser.eval.Eval;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.PrimitiveValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;

class SubTuple {
	PrimitiveValue val;
	int offset;
	
	public SubTuple(PrimitiveValue val, int offset) {
		this.val = val;
		this.offset = offset;
	}
}

class MergeTuple {
	PrimitiveValue val;
	int offset;
	int pgNum;
	
	public MergeTuple(PrimitiveValue val, int offset, int pgNum) {
		this.val = val;
		this.offset = offset;
		this.pgNum = pgNum;
	}
}

public class MyCreateTable {
	private CreateTable table;
	private Map<String, Integer> colNameMap;
	private Map<String, String> dataTypeMap = new HashMap<String, String>();
	private List<ColumnDefinition> columnDefinitions;

	private Eval eval = new Eval() {
		
		@Override
		public PrimitiveValue eval(Column arg0) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
	};
	
	private PriorityQueue<MergeTuple> sortedPq = new PriorityQueue<MergeTuple>(new Comparator<MergeTuple>()  {
		public int compare(MergeTuple s1, MergeTuple s2) {
			try {
				if (eval.eval(new MinorThan(s1.val, s2.val)).toBool()) {
					return -1;
				}
				else if (eval.eval(new GreaterThan(s1.val, s2.val)).toBool()) {
					return 1;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
	});
	
	public MyCreateTable(Statement query) {
		this.table = (CreateTable) query;
	}
	
	public CreateTable getTable() {
		return this.table;
	}
	
	public String getTableName() {
		return table.getTable().getName();
	}
	
	public Map<String, Integer> getSchemaDetails(CreateTable table) {
		colNameMap = new HashMap<String, Integer>();
		columnDefinitions = table.getColumnDefinitions();
		
		List<ColumnDefinition> dataTypesList = new ArrayList<ColumnDefinition>();
		dataTypesList = table.getColumnDefinitions();
		
        for (int i = 0; i < columnDefinitions.size(); i++) {
        	String dataType = dataTypesList.get(i).getColDataType().toString();
        	if (dataType.startsWith("CHAR")) {
        		dataType = "CHAR";
        	}
        	colNameMap.put(columnDefinitions.get(i).getColumnName(), i);
        	dataTypeMap.put(columnDefinitions.get(i).getColumnName().toString(), dataType);
        }
        
        return colNameMap;
	}

	public Map<String, String> getDatatypes() {
		return dataTypeMap;
	}
	
	public List<Index> getIndexes() {
		return table.getIndexes();
	}
	
	public List<TreeMap<PrimitiveValue, Integer>> getIndexMap(List<Index> indexes) throws IOException {
		List<TreeMap<PrimitiveValue, Integer>> indexList = new ArrayList<TreeMap<PrimitiveValue, Integer>>();
		int indCnt = 0;
		for (Index index : indexes) {
			File file = new File("data/"+table.getTable().getName()+".csv");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String tuple = "";
			if(index.getName() == null) {
				continue;
			}
			String colName = index.getColumnsNames().get(indCnt++).toUpperCase();
			
			File directory = new File(colName+"index");
			 if (! directory.exists()){
			        directory.mkdir();
			 }
			
			List<SubTuple> temp = new ArrayList<SubTuple>();
			int rowOffset = 0;
			int fileCount = 0;
			while ((tuple = br.readLine()) != null) {
				StringTokenizer st = new StringTokenizer(tuple, "\\|");
				String[] tupleList = new String[columnDefinitions.size()];
				for (int i = 0; i < tupleList.length; i++) {
					tupleList[i] = st.nextToken();
				}
				
				int pos = colNameMap.get(colName);
				String dataType = dataTypeMap.get(colName);
				PrimitiveValue val = null;
				
				if(dataType.equalsIgnoreCase("int")){
					val = new LongValue(Long.parseLong(tupleList[pos]));
				}
				
				else if(dataType.equalsIgnoreCase("decimal")){
					val = new DoubleValue(Double.parseDouble(tupleList[pos]));
				}
				
				else if(dataType.equalsIgnoreCase("date")){
					val = new DateValue(tupleList[pos]);
				}
				
				else if (dataType.equalsIgnoreCase("String") || dataType.equalsIgnoreCase("varchar") || dataType.equalsIgnoreCase("char")){
					val = new StringValue(tupleList[pos]);
				}
				
				SubTuple subTuple = new SubTuple(val, rowOffset);
				rowOffset += tuple.length() + 1;
				temp.add(subTuple);
				
				if (temp.size() > 10000) {
					// Flush
					
					Collections.sort(temp, new Comparator<SubTuple>() {
						public int compare(SubTuple s1, SubTuple s2) {
							try {
								if (eval.eval(new MinorThan(s1.val, s2.val)).toBool()) {
									return -1;
								}
								else if (eval.eval(new GreaterThan(s1.val, s2.val)).toBool()) {
									return 1;
								}
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
							return 0;
						}
					});
					

					Path loc = Paths.get(colName+"index/"+fileCount+".csv");
					fileCount++;
					StringBuilder fWrite = new StringBuilder();
					for (int i = 0; i < temp.size(); i++) {
						SubTuple tmp = temp.get(i);
						fWrite.append(tmp.val.toRawString()+"|"+tmp.offset +"\n");
					}
					
					Files.write(loc, fWrite.toString().getBytes());
					temp.clear();
				}
			}
			br.close();
			
			// force flush

			Collections.sort(temp, new Comparator<SubTuple>() {
				public int compare(SubTuple s1, SubTuple s2) {
					try {
						if (eval.eval(new MinorThan(s1.val, s2.val)).toBool()) {
							return -1;
						}
						else if (eval.eval(new GreaterThan(s1.val, s2.val)).toBool()) {
							return 1;
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					return 0;
				}
			});
			
			StringBuilder fWrite = new StringBuilder();
			for (int i = 0; i < temp.size(); i++) {
				SubTuple tmp = temp.get(i);
				fWrite.append(tmp.val.toRawString()+"|"+tmp.offset+"\n");
			}
			
			Path loc = Paths.get(colName+"index/"+fileCount+".csv");
			Files.write(loc, fWrite.toString().getBytes());
			fileCount++;
			
			//Do Merge
			String newLine;
			String outputLine;
			int numPages = 1;
			if (fileCount > 1) {
				numPages = fileCount;
			}
			
			BufferedReader[] brArray = new BufferedReader[numPages];
			BufferedWriter bw = new BufferedWriter(new FileWriter(colName+"index/temp_sorted.csv"));
			for (int i = 0; i < numPages; i++) {
				brArray[i] = new BufferedReader(new FileReader(colName+"index/"+i+".csv"));
				newLine = brArray[i].readLine();
				//System.out.println(newLine);
				String[] tupleList = newLine.split("\\|");
				if(newLine != null) {
					PrimitiveValue val = null;
					int offset = Integer.parseInt(tupleList[1]);
					String dType = dataTypeMap.get(colName);
					
					if(dType.equalsIgnoreCase("int")){
						val = new LongValue(Long.parseLong(tupleList[0]));
					}
					
					else if(dType.equalsIgnoreCase("decimal")){
						val = new DoubleValue(Double.parseDouble(tupleList[0]));
					}
					
					else if(dType.equalsIgnoreCase("date")){
						val = new DateValue(tupleList[0]);
					}
					
					else if (dType.equalsIgnoreCase("String") || dType.equalsIgnoreCase("varchar") || dType.equalsIgnoreCase("char")){
						val = new StringValue(tupleList[0]);
					}
					
					MergeTuple newTuple = new MergeTuple(val, offset, i);
					//System.out.println(sortedPq.size());
					sortedPq.add(newTuple);
				}
			}
			
			while(!sortedPq.isEmpty()) {
				MergeTuple obj = sortedPq.poll();
				StringBuilder toWrite = new StringBuilder();
				toWrite.append(obj.val+"|"+obj.offset);
				bw.write(toWrite + "\n");
				
				if((outputLine = brArray[obj.pgNum].readLine()) != null) {
					String[] tupleList = outputLine.split("\\|");
					
					PrimitiveValue val = null;
					int offset = Integer.parseInt(tupleList[1]);
					String dType = dataTypeMap.get(colName);
					
					if(dType.equalsIgnoreCase("int")){
						val = new LongValue(Long.parseLong(tupleList[0]));
					}
					
					else if(dType.equalsIgnoreCase("decimal")){
						val = new DoubleValue(Double.parseDouble(tupleList[0]));
					}
					
					else if(dType.equalsIgnoreCase("date")){
						val = new DateValue(tupleList[0]);
					}
					
					else if (dType.equalsIgnoreCase("String") || dType.equalsIgnoreCase("varchar") || dType.equalsIgnoreCase("char")){
						val = new StringValue(tupleList[0]);
					}
					
					MergeTuple nextTuple = new MergeTuple(val, offset, obj.pgNum);
					sortedPq.offer(nextTuple);
				}
			}
			
			bw.close();
		}
		
		indCnt = 0;
		for (Index index: indexes) {
			if (index.getName() == null) {
				continue;
			}
			String colName = index.getColumnsNames().get(indCnt++).toUpperCase();
			String grpBy = "";
			File file = new File(colName+"index/temp_sorted.csv");
			BufferedReader br = new BufferedReader(new FileReader(file));
			String currLine = "";
			StringBuilder offsets = new StringBuilder();
			BufferedWriter bw = new BufferedWriter(new FileWriter(colName+"index/sorted.csv"));
			while ((currLine = br.readLine()) != null) {
				int pipe = currLine.indexOf('|');
				String local = currLine.substring(0, pipe);
				String offsetVal = currLine.substring(pipe + 1);
				
				if (grpBy.equals("")) {
					grpBy = local;
					offsets.append(offsetVal+"$");
				}
				else if (grpBy.equals(local)) {
					offsets.append(offsetVal+"$");
				}
				else {
					offsets.deleteCharAt(offsets.length() - 1);
					offsets.append("\n");
					bw.write(grpBy+"|"+offsets.toString());
					grpBy = local;
					offsets = new StringBuilder();
					offsets.append(offsetVal+"$");
				}
			}
			
			if (offsets.length() > 0) {
				offsets.deleteCharAt(offsets.length() - 1);
				offsets.append("\n");
				bw.write(grpBy+"|"+offsets.toString());
				offsets = new StringBuilder();
			}
			br.close();
			bw.close();
			
			file = new File(colName+"index/sorted.csv");
			br = new BufferedReader(new FileReader(file));
			currLine = "";
			int newOffset = 0;
			TreeMap<PrimitiveValue, Integer> miniMap = new TreeMap<PrimitiveValue, Integer>(new Comparator<PrimitiveValue>() {
				public int compare(PrimitiveValue p1, PrimitiveValue p2) {
					try {
						if (eval.eval(new MinorThan(p1, p2)).toBool()) {
							return -1;
						}
						else if (eval.eval(new GreaterThan(p1, p2)).toBool()) {
							return 1;
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return 0;
				}
			}); 
			
			while ((currLine = br.readLine()) != null) {
				int pipeIndex = currLine.indexOf("|");
				String key = currLine.substring(0, pipeIndex);
				String val = currLine.substring(pipeIndex + 1);
				String dType = dataTypeMap.get(colName);
				PrimitiveValue pKey = null;
				
				if(dType.equalsIgnoreCase("int")){
					pKey = new LongValue(Long.parseLong(key));
				}
				
				else if(dType.equalsIgnoreCase("decimal")){
					pKey = new DoubleValue(Double.parseDouble(key));
				}
				
				else if(dType.equalsIgnoreCase("date")){
					pKey = new DateValue(key);
				}
				
				else if (dType.equalsIgnoreCase("String") || dType.equalsIgnoreCase("varchar") || dType.equalsIgnoreCase("char")){
					pKey = new StringValue(key);
				}
				miniMap.put(pKey, newOffset);
				newOffset += val.length();
			}
			indexList.add(miniMap);
		}
		return indexList;
	}
	
	public List<String> getCsvList(String csv) throws IOException {
		List<String> csvList = new ArrayList<String>();
		File file = new File("data/"+csv);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = "";
		
		while ((line = br.readLine()) != null) {
			csvList.add(line);
		}
		br.close();
		return csvList;
	}
}
