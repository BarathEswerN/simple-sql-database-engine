package dubstep;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

import net.sf.jsqlparser.eval.Eval;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.PrimitiveValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.select.SelectItem;

class Tuple {
	PrimitiveValue[] primArr;
	int fileNum;
	public Tuple(PrimitiveValue[] primArr, int fileNum) {
		this.primArr = primArr;
		this.fileNum = fileNum;
	}
}

public class MyGroupBy {
	PrimitiveValue currVal;
	private int fileCount = 0;
	List<Column> groupByElem;
	PrimitiveValue[] input;
	Map<String, String> dataTypeMap;
	Map<String, Integer> colNameMap;
	static int count = 0;

	private Eval eval = new Eval() {
		
		@Override
		public PrimitiveValue eval(Column arg0) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
	};
	private PriorityQueue<PrimitiveValue[]> pq = new PriorityQueue<PrimitiveValue[]>(new Comparator<PrimitiveValue[]>() {
		public int compare(PrimitiveValue[] p1, PrimitiveValue[] p2) {
			for (int i = 0; i < groupByElem.size(); i++) {
				try {
					String colName = groupByElem.get(i).toString();
					int index = colNameMap.get(colName);
					if (dataTypeMap.get(colName).equals("CHAR") || dataTypeMap.get(colName).equals("STRING") || dataTypeMap.get(colName).equals("VARCHAR")) {
						if (!(p1[index].toString().compareTo(p2[index].toString()) == 0)) {
							return p1[index].toString().compareTo(p2[index].toString());
						}
					}
					else {
						if (eval.eval(new MinorThan(p1[index], p2[index])).toBool()) {
							return -1;
						}
						else if (eval.eval(new GreaterThan(p1[index], p2[index])).toBool()) {
							return 1;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			return 0;
		}
	});
	
	private PriorityQueue<Tuple> sortPq = new PriorityQueue<Tuple>(new Comparator<Tuple>() {
		public int compare(Tuple p1, Tuple p2) {
			for (int i = 0; i < groupByElem.size(); i++) {
				try {
					String colName = groupByElem.get(i).toString();
					int index = colNameMap.get(colName);
					if (dataTypeMap.get(colName).equals("CHAR") || dataTypeMap.get(colName).equals("STRING") || dataTypeMap.get(colName).equals("VARCHAR")) {
						if (!(p1.primArr[index].toString().compareTo(p2.primArr[index].toString()) == 0)) {
							return p1.primArr[index].toString().compareTo(p2.primArr[index].toString());
						}
					}
					else {
						if (eval.eval(new MinorThan(p1.primArr[index], p2.primArr[index])).toBool()) {
							return -1;
						}
						else if (eval.eval(new GreaterThan(p1.primArr[index], p2.primArr[index])).toBool()) {
							return 1;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			return p1.fileNum - p2.fileNum;
		}
	});
	
	public MyGroupBy(List<Column> groupByElem, Map<String, Integer> colNameMap, Map<String, String> dataTypeMap) {
		this.groupByElem = groupByElem;
		this.colNameMap = colNameMap;
		this.dataTypeMap = dataTypeMap;
		File directory = new File("temp");
	    if (! directory.exists()){
	        directory.mkdir();
	    }
	}
	
	public void applyAggregate(List<SelectItem> proj, Map<String, Integer> colMap, Map<String, String> dataTypeMap, List<Column> grpByElem, int rowCnt) throws IOException, SQLException {
		File file = new File("temp/sorted.csv");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String tuple = "";
		String grpByTerm = "";
		EvalWhere ew = new EvalWhere();
		PrimitiveValue[] primValues = new PrimitiveValue[proj.size()];

		while ((tuple = br.readLine()) != null && rowCnt > 0) {
			StringTokenizer st = new StringTokenizer(tuple, "\\|");
			String[] tupleList = new String[colMap.size()];
			for (int i = 0; i < tupleList.length; i++) {
				tupleList[i] = st.nextToken();
			}
			String curGrpBy = "";
			for (Column col : grpByElem) {
				int index = colMap.get(col.toString());
				//System.out.println("IND "+index);
				curGrpBy += tupleList[index] +"|";
			}
			if (grpByTerm.length() == 0) {
				grpByTerm = curGrpBy;
				primValues = ew.getOuterSelectAggr(tuple, primValues, proj, colMap, dataTypeMap);
				count++;
			}
			
			else if (grpByTerm.equals(curGrpBy)) {
				primValues = ew.getOuterSelectAggr(tuple, primValues, proj, colMap, dataTypeMap);
				count++;
			}
			
			else {
				primValues = ew.evalAvg(primValues, proj, count);
				int index = 0;
				for (PrimitiveValue pv : primValues) {
					if (pv != null) {
						if (index == 0) {
							System.out.print(pv.toString());
							index++;
						}
						else {
							System.out.print("|"+pv.toString());
						}
					}
				}
				System.out.println();
				primValues = new PrimitiveValue[proj.size()];
				primValues = ew.getOuterSelectAggr(tuple, primValues, proj, colMap, dataTypeMap);
				count = 1;
				grpByTerm = curGrpBy;
			}
			rowCnt--;
		}
		// To flush the primitive value array
		
		if (rowCnt > 0) {
			primValues = ew.evalAvg(primValues, proj, count);
			count = 0;
			int index = 0;
			for (PrimitiveValue pv : primValues) {
				if (pv != null) {
					if (index == 0) {
						System.out.print(pv.toString());
						index++;
					}
					else {
						System.out.print("|"+pv.toString());
					}
				}
			}
			System.out.println();
		}
		br.close();
	}
	
	public PrimitiveValue[] getPrimitiveArr(String tuple, String[] secSelect, Map<String, String> dataTypeMap, List<ColumnDefinition>colDef) {
		boolean isSecValid = true;
		int length = secSelect.length;
		if (length == 0) {
			length = colDef.size();
			isSecValid = false;
		}
		PrimitiveValue[] arr = new PrimitiveValue[length];

		StringTokenizer st = new StringTokenizer(tuple, "\\|");
		String[] tupleList = new String[length];
		for (int i = 0; i < tupleList.length; i++) {
			tupleList[i] = st.nextToken();
		}
		
		for (int i = 0; i < tupleList.length; i++) {
			String colName = "";
			if (isSecValid)
				colName = secSelect[i];
			else {
				colName = colDef.get(i).getColumnName();
			}
			String dType = dataTypeMap.get(colName);
			if(dType.equalsIgnoreCase("int")){
				arr[i] = new LongValue(Long.parseLong(tupleList[i]));
			}
			
			else if(dType.equalsIgnoreCase("decimal")){
				arr[i] = new DoubleValue(Double.parseDouble(tupleList[i]));
			}
			
			else if(dType.equalsIgnoreCase("date")){
				arr[i] = new DateValue(tupleList[i]);
			}
			
			else if (dType.equalsIgnoreCase("String") || dType.equalsIgnoreCase("varchar") || dType.equalsIgnoreCase("char")){
				arr[i] = new StringValue(tupleList[i]);
			}
		}

		return arr;
	}
	public void evalGroupBy(PrimitiveValue[] ip) throws IOException {
		this.input = ip;
		pq.offer(ip);
		if (pq.size() > 1000) {
			Path file = Paths.get("temp/"+fileCount+".csv");
			fileCount++;
			StringBuilder fWrite = new StringBuilder();
			while (pq.size() > 0) {
				PrimitiveValue[] tmp = pq.poll();
				int len = tmp.length;
				for (int i = 0; i < tmp.length; i++) {
					if (i != len - 1) {
						fWrite.append(tmp[i].toRawString()+"|");
					}
					else {
						fWrite.append(tmp[i].toRawString()+"\n");
					}
				}
			}
			Files.write(file, fWrite.toString().getBytes());
		}
	}
	
	public void pqFlush() throws IOException {
		StringBuilder fWrite = new StringBuilder();
		while(pq.size() > 0) {
			PrimitiveValue[] tmp = pq.poll();
			int len = tmp.length;
			for (int i = 0; i < tmp.length; i++) {
				if (i != len - 1) {
					fWrite.append(tmp[i].toRawString()+"|");
				}
				else {
					fWrite.append(tmp[i].toRawString()+ "\n");
				}
			}
		}
		Path file = Paths.get("temp/"+fileCount+".csv");
		Files.write(file, fWrite.toString().getBytes());
		fileCount++;
	}
	
	public void mergeFiles(String[] secSelect, Map<String, String> dataTypeMap, List<ColumnDefinition> colDef) throws IOException {
		String tuple;
		String outputLine;
		int numPages = 0;
		if (fileCount > 1) {
			numPages = fileCount;
		}
		else {
			numPages = 1;
		}
		BufferedReader[] brArray = new BufferedReader[numPages];
		BufferedWriter bw = new BufferedWriter(new FileWriter("temp/sorted.csv"));
		for (int i = 0; i < numPages; i++) {
			brArray[i] = new BufferedReader(new FileReader("temp/"+i+".csv"));
			tuple = brArray[i].readLine();
			if(tuple != null) {
				PrimitiveValue[] arr = getPrimitiveArr(tuple, secSelect, dataTypeMap, colDef);
				Tuple obj = new Tuple(arr, i);
				sortPq.offer(obj);
			}
		}
		while(!sortPq.isEmpty()) {
			//System.out.println("poll "+sortPq.peek().primArr[0].toString());
			Tuple obj = sortPq.poll();
			StringBuilder toWrite = new StringBuilder();
			for (int i = 0; i < obj.primArr.length; i++) {
				if (i != obj.primArr.length - 1) {
					toWrite.append(obj.primArr[i].toRawString() +"|");
				}
				else {
					toWrite.append(obj.primArr[i].toRawString());
				}
			}
			//System.out.println("IN FILE "+toWrite);
			bw.write(toWrite + "\n");
			
			if((outputLine = brArray[obj.fileNum].readLine()) != null) {
				PrimitiveValue[] arr = getPrimitiveArr(outputLine, secSelect, dataTypeMap, colDef);
				Tuple nextTuple = new Tuple(arr, obj.fileNum);
				sortPq.offer(nextTuple);
			}
		}
		bw.close();
	}
}
