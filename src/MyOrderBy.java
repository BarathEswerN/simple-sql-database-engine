package dubstep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.StringTokenizer;

import net.sf.jsqlparser.eval.Eval;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.PrimitiveValue;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.SelectItem;

public class MyOrderBy {
	PrimitiveValue currVal;
	private int fileCount = 0;
	List<OrderByElement> orderByElem;
	PrimitiveValue[] input;
	Map<String, String> dataTypeMap;
	Map<String, Integer> colNameMap;
	private Eval eval = new Eval() {
		
		@Override
		public PrimitiveValue eval(Column arg0) throws SQLException {
			// TODO Auto-generated method stub
			return null;
		}
	};
	private PriorityQueue<PrimitiveValue[]> pq = new PriorityQueue<PrimitiveValue[]>(new Comparator<PrimitiveValue[]>() {
		public int compare(PrimitiveValue[] p1, PrimitiveValue[] p2) {
			for (int i = 0; i < orderByElem.size(); i++) {
				try {
					String colName = orderByElem.get(i).toString();
					boolean isDesc = false;
					if (colName.contains("DESC")) {
						int spaceIndex = colName.indexOf(' ');
						colName = colName.substring(0, spaceIndex);
						isDesc = true;
					}
					else {
						isDesc = false;
					}
					int index = colNameMap.get(colName);
					if (dataTypeMap.get(colName).equals("CHAR") || dataTypeMap.get(colName).equals("STRING") || dataTypeMap.get(colName).equals("VARCHAR")) {
						if (!(p1[index].toString().compareTo(p2[index].toString()) == 0)) {
							if (isDesc) {
								return p2[index].toString().compareTo(p1[index].toString());
							}
							return p1[index].toString().compareTo(p2[index].toString());
						}
					}
					else {
						if (eval.eval(new MinorThan(p1[index], p2[index])).toBool()) {
							if (isDesc) {
								return 1;
							}
							return -1;
						}
						else if (eval.eval(new GreaterThan(p1[index], p2[index])).toBool()) {
							if (isDesc) {
								return -1;
							}
							return 1;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 0;
		}
	});
	
	private PriorityQueue<Tuple> sortPq = new PriorityQueue<Tuple>(new Comparator<Tuple>() {
		public int compare(Tuple p1, Tuple p2) {
			//System.out.println("P1 length "+p1.primArr.length);
			//System.out.println("G SIZE "+groupByElem.size());
			for (int i = 0; i < orderByElem.size(); i++) {
				try {
					String colName = orderByElem.get(i).toString();
					boolean isDesc = false;
					if (colName.contains("DESC")) {
						int spaceIndex = colName.indexOf(' ');
						colName = colName.substring(0, spaceIndex);
						isDesc = true;
					}
					else {
						isDesc = false;
					}
					int index = colNameMap.get(colName);
					if (dataTypeMap.get(colName).equals("CHAR") || dataTypeMap.get(colName).equals("STRING") || dataTypeMap.get(colName).equals("VARCHAR")) {
						if (!(p1.primArr[index].toString().compareTo(p2.primArr[index].toString()) == 0)) {
							if (isDesc) {
								return p2.primArr[index].toString().compareTo(p1.primArr[index].toString());
							}
							return p1.primArr[index].toString().compareTo(p2.primArr[index].toString());
						}
					}
					else {
						if (eval.eval(new MinorThan(p1.primArr[index], p2.primArr[index])).toBool()) {
							if (isDesc) {
								return 1;
							}
							return -1;
						}
						else if (eval.eval(new GreaterThan(p1.primArr[index], p2.primArr[index])).toBool()) {
							if (isDesc) {
								return -1;
							}
							return 1;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			return p1.fileNum - p2.fileNum;
		}
	});
	
	public PrimitiveValue[] getPrimitiveArr(String tuple, String[] secSelect, Map<String, String> dataTypeMap, List<ColumnDefinition>colDef) {
		boolean isSecValid = true;
		//System.out.print("Tuple "+tuple +secSelect.length);
		int length = secSelect.length;
		//System.out.println("Size "+colDef);
		if (length == 0) {
			length = colDef.size();
			isSecValid = false;
		}
		PrimitiveValue[] arr = new PrimitiveValue[length];

		StringTokenizer st = new StringTokenizer(tuple, "\\|");
		String[] tupleList = new String[length];
		for (int i = 0; i < tupleList.length; i++) {
			tupleList[i] = st.nextToken();
		}
		
		for (int i = 0; i < tupleList.length; i++) {
			String colName = "";
			if (isSecValid)
				colName = secSelect[i];
			else {
				colName = colDef.get(i).getColumnName();
			}
			String dType = dataTypeMap.get(colName);
			if(dType.equalsIgnoreCase("int")){
				arr[i] = new LongValue(Long.parseLong(tupleList[i]));
			}
			
			if(dType.equalsIgnoreCase("decimal")){
				arr[i] = new DoubleValue(Double.parseDouble(tupleList[i]));
			}
			
			if(dType.equalsIgnoreCase("date")){
				arr[i] = new DateValue(tupleList[i]);
			}
			
			if (dType.equalsIgnoreCase("String") || dType.equalsIgnoreCase("varchar") || dType.equalsIgnoreCase("char")){
				arr[i] = new StringValue(tupleList[i]);
			}
		}

		return arr;
	}
	
	public MyOrderBy(List<OrderByElement> orderByElem, Map<String, Integer> colMap, Map<String, String> dataTypeMap) {
		this.orderByElem = orderByElem;
		this.colNameMap = colMap;
		this.dataTypeMap = dataTypeMap;
		File directory = new File("orderBy");
	    if (! directory.exists()){
	        directory.mkdir();
	    }
	}
	
	public void evalOrderBy(PrimitiveValue[] ip) throws IOException {
		this.input = ip;
		pq.offer(ip);
		if (pq.size() > 1000) {
			//Create new File and Flush it
			Path file = Paths.get("orderBy/"+fileCount+".csv");
			fileCount++;
			StringBuilder fWrite = new StringBuilder();
			while (pq.size() > 0) {
				PrimitiveValue[] tmp = pq.poll();
				int len = tmp.length;
				for (int i = 0; i < tmp.length; i++) {
					if (i != len - 1) {
						fWrite.append(tmp[i].toRawString()+"|");
					}
					else {
						fWrite.append(tmp[i].toRawString()+"\n");
					}
				}
			}
			//System.out.println("FIN STRING "+fWrite);
			Files.write(file, fWrite.toString().getBytes());
		}
	}
	
	public void pqFlush() throws IOException {
		StringBuilder fWrite = new StringBuilder();
		while(pq.size() > 0) {
			PrimitiveValue[] tmp = pq.poll();
			int len = tmp.length;
			for (int i = 0; i < tmp.length; i++) {
				if (i != len - 1) {
					fWrite.append(tmp[i].toRawString()+"|");
				}
				else {
					fWrite.append(tmp[i].toRawString()+ "\n");
				}
			}
		}
		Path file = Paths.get("orderBy/"+fileCount+".csv");
		Files.write(file, fWrite.toString().getBytes());
		fileCount++;
	}
	
	public void mergeFiles(String[] secSelect, Map<String, String> dataTypeMap, List<ColumnDefinition> colDef, List<SelectItem> outerProj, Map<String, Integer> colNameMap, int rowCnt) throws IOException {
		int ramThreshold = 1024000;
		String tuple;
		String outputLine;
		int readBytes = 0;
		int numPages = 0;
		//System.out.print("FILE CNT "+fileCount);
		if (fileCount > 1) {
			numPages = fileCount;
		}
		else {
			numPages = 1;
		}
		BufferedReader[] brArray = new BufferedReader[numPages];
		for (int i = 0; i < numPages; i++) {
			brArray[i] = new BufferedReader(new FileReader("orderBy/"+i+".csv"));
			readBytes = 0;
			tuple = brArray[i].readLine();
			while(tuple != null && readBytes + tuple.length() < (ramThreshold / numPages)) {
				PrimitiveValue[] arr = getPrimitiveArr(tuple, secSelect, dataTypeMap, colDef);
				Tuple obj = new Tuple(arr, i);
				sortPq.offer(obj);
				readBytes += tuple.length();
				tuple = brArray[i].readLine();
			}
			if(tuple != null) {
				PrimitiveValue[] arr = getPrimitiveArr(tuple, secSelect, dataTypeMap, colDef);
				Tuple obj = new Tuple(arr, i);
				sortPq.offer(obj);
			}
		}
		while(!sortPq.isEmpty() && rowCnt > 0) {
			Tuple obj = sortPq.poll();
			if (!(outerProj.get(0) instanceof AllColumns)) {
				for (int i = 0; i < outerProj.size(); i++) {
					String colName = outerProj.get(i).toString();
					int index = colNameMap.get(colName);
					if (i != outerProj.size() - 1) {
						System.out.print(obj.primArr[index].toString() +"|");
					}
					else {
						System.out.print(obj.primArr[index].toString());
					}
				}
			System.out.println();
			}
			else {
				for (int i = 0; i < obj.primArr.length; i++) {
					if (i != (obj.primArr.length - 1))
						System.out.print(obj.primArr[i] + "|");
					else 
						System.out.print(obj.primArr[i]);
				}
				System.out.println();
			}
			if((outputLine = brArray[obj.fileNum].readLine()) != null) {
				PrimitiveValue[] arr = getPrimitiveArr(outputLine, secSelect, dataTypeMap, colDef);
				Tuple nextTuple = new Tuple(arr, obj.fileNum);
				sortPq.offer(nextTuple);
			}
			rowCnt--;
		}
	}	
}
