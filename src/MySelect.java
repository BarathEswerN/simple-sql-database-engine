package dubstep;

import java.util.*;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Limit;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SubSelect;

public class MySelect {
	private Stack<List<SelectItem>> selectStk = new Stack<List<SelectItem>>();
	private Stack<Expression> whereStk = new Stack<Expression>();
	private Queue<List<SelectItem>> selectQ = new LinkedList<List<SelectItem>>();
	private Queue<Expression> whereQ = new LinkedList<Expression>();
	private FromItem tableName = null;
	private List<OrderByElement> orderElem = null;
	private List<Column> groupElem = null;
	private boolean isOuterSelect = true;
	private List<SelectItem> outerSelect;
	private Expression outerWhr;
	private Limit limit;
	public MySelect(Statement query) throws JSQLParserException {
	
		if (query instanceof Select) {
			 Select select = (Select)query;
			    SelectBody selectBody = select.getSelectBody();
			    if (selectBody != null) {
			    	while (selectBody != null) {
				    	if (selectBody instanceof PlainSelect) {
				    		//System.out.println(selectBody);
				    		//this.select = (PlainSelect) selectBody;
				    		PlainSelect pm = (PlainSelect)selectBody;
				    		if (isOuterSelect) {
				    			isOuterSelect = false;
				    			outerSelect = pm.getSelectItems();
				    			outerWhr = pm.getWhere();
				    		}
				    		else {
					    		selectStk.push(pm.getSelectItems());
					    		whereStk.push(pm.getWhere());
				    		}
				    		tableName = pm.getFromItem();
				    		if (orderElem == null) {
				    			orderElem = pm.getOrderByElements();
				    		}
				    		
				    		if (groupElem == null) {
				    			groupElem = pm.getGroupByColumnReferences();
				    		}
				    		
				    		if (limit == null) {
				    			limit = pm.getLimit();
				    		}
				    		if (!(tableName instanceof Table)) {
					    		SubSelect sb = (SubSelect)tableName;
				    			selectBody = sb.getSelectBody();
				    			//System.out.println("SB "+selectBody);
				    		}
				    		else {
				    			break;
				    		}
				    	}
			    	}
			    } 
			    while (selectStk != null && selectStk.size() > 0) {
			    	selectQ.offer(selectStk.pop());
			    	whereQ.offer(whereStk.pop());
			    }
			    selectQ.offer(null);
			    whereQ.offer(null);

		}
		else {
			throw new JSQLParserException("I can't understand query" +query);
		}

	}
	
	public FromItem getTableName() {
		return tableName;
	}
	
	public List<SelectItem> getOuterSelect() {
		return outerSelect;
	}
	
	public Expression getOuterWhere() {
		return outerWhr;
	}
	
	public boolean hasNext() {
//		System.out.println("Sel  "+selectQ.peek());
//		System.out.println("Whr  "+whereQ.peek());

		return (selectQ.size() > 0);
		
	}
	
	public List<SelectItem> getSelectItems() {
		List<SelectItem> temp= selectQ.poll();
		selectQ.offer(temp);
		return temp;	
	}
	
	public Expression getWhereItems() {
		Expression temp = whereQ.poll();
		whereQ.offer(temp);
		return temp;		
	}

	public List<Column> getGroupByElements() {
		return groupElem;
	}
	
	public List<OrderByElement> getOrderByElements() {
		return orderElem;
	}
	
	public Limit getLimit() {
		return limit;
	}
	
}
