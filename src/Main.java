package dubstep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jsqlparser.JSQLParserException; 
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.PrimitiveValue;
import net.sf.jsqlparser.expression.PrimitiveValue.InvalidPrimitive;
import net.sf.jsqlparser.parser.*; 
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement; 
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.statement.select.Limit;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectItem;

public class Main {
	
	static MyCreateTable myTable;
	static List<ColumnDefinition> columnDefinitions = new ArrayList<ColumnDefinition>();
	static List<Column> groupByElem;
	static List<SelectItem> selectItems;
	static List<OrderByElement> orderByElem;
	static Expression whereItem;
	static Limit limit;
	static List<Index> indexes;
	static String tableName;
	static Map<String, Integer> colMap = new HashMap<String, Integer>();
	static Map<String, String> dataTypeMap = new HashMap<String, String>();
	static List<String> csvList = new ArrayList<String>();
	static boolean isInMem = false;
	//static long totalTime = 0;
	
	public static void printTableQueris(String tableName, Map<String, Integer>  colNameMap, Map<String, String> dataTypeMap, MySelect mySelect) throws JSQLParserException, IOException, InvalidPrimitive, SQLException {
		// Parse the folder
		File file = new File("data/"+tableName);
		BufferedReader br = new BufferedReader(new FileReader(file));
        EvalWhere evalWhr = new EvalWhere();
        String tuple = "";
        boolean isValidTuple = false;
        boolean hasNested = false;
        String[] currSelect = new String[0];
        MyGroupBy grpBy = new MyGroupBy(groupByElem, colNameMap, dataTypeMap);
        MyOrderBy ordBy = new MyOrderBy(orderByElem, colNameMap, dataTypeMap);
        Map<String, Integer> projMap = new HashMap<String, Integer>();
        int rowCnt = Integer.MAX_VALUE;
		if (limit != null)
			rowCnt = (int) limit.getRowCount();
        PrintTable pt = new PrintTable(rowCnt);
		List<SelectItem> outerProj = mySelect.getOuterSelect();

    	PrimitiveValue[] primValues = new PrimitiveValue[outerProj.size()];;
    	if (isInMem) {
    		//System.out.println("In MEM");
    		for (String str : csvList){
    			tuple = str;
    			for (String key : colNameMap.keySet()) {
    				projMap.put(key, colNameMap.get(key));
    			}

    			while (mySelect.hasNext()) {
    				List<SelectItem> projection = mySelect.getSelectItems();
    				whereItem = mySelect.getWhereItems(); 
    				if (projection == null) {
    					break;
    				}
    				hasNested = true;
    				
    				if (whereItem == null) {
    					isValidTuple = true;
    				}
    				
    				else {
    					if (evalWhr.evalWhere(whereItem, tuple, projMap, dataTypeMap)) {
    						isValidTuple = true;
    					}
    					else {
    						isValidTuple = false;
    						// Structure the queue 
    						while(mySelect.getSelectItems() != null) {
    							whereItem = mySelect.getWhereItems();
    						}
    						whereItem = mySelect.getWhereItems();
    						break;
    					}
    				}
    			}
    			
    			if ((!hasNested) || isValidTuple) {
    				//System.out.println("Entered");
    				Expression outerWhr = mySelect.getOuterWhere();
    				if (outerWhr != null) {
    					if (evalWhr.evalWhere(outerWhr, tuple, projMap, dataTypeMap)) {
    						isValidTuple = true;
    					}
    					else {
    						isValidTuple = false;
    					}
    				}
    				else {
    					isValidTuple = true;
    				}
    			}
    			//System.out.println(isValidTuple);
    			if ((isValidTuple) && (groupByElem != null)) {
    				//System.out.println(tuple);
    				PrimitiveValue[] whrRes = evalWhr.getPrimArr(tuple, dataTypeMap, columnDefinitions);
    				
    				//System.out.println(whrRes[0]);
    				grpBy.evalGroupBy(whrRes);
    			}
    			
    			else if ((isValidTuple) && ((groupByElem == null) && (orderByElem != null))) {
    				PrimitiveValue[] whrRes = evalWhr.getPrimArr(tuple, dataTypeMap, columnDefinitions);
    				ordBy.evalOrderBy(whrRes);
    			}
    			
    			else if (isValidTuple) {
    				primValues = pt.printTuples(outerProj, tuple, colNameMap, dataTypeMap, primValues);
    			}
    		}
    	}
    	else {
			while ((tuple = br.readLine()) != null) {
				for (String key : colNameMap.keySet()) {
					projMap.put(key, colNameMap.get(key));
				}
	
				while (mySelect.hasNext()) {
					List<SelectItem> projection = mySelect.getSelectItems();
					whereItem = mySelect.getWhereItems(); 
					if (projection == null) {
						break;
					}
					hasNested = true;
					
					if (whereItem == null) {
						isValidTuple = true;
					}
					
					else {
						if (evalWhr.evalWhere(whereItem, tuple, projMap, dataTypeMap)) {
							isValidTuple = true;
						}
						else {
							isValidTuple = false;
							// Structure the queue 
							while(mySelect.getSelectItems() != null) {
								whereItem = mySelect.getWhereItems();
							}
							whereItem = mySelect.getWhereItems();
							break;
						}
					}
				}
				
				if ((!hasNested) || isValidTuple) {
					//System.out.println("Entered");
					Expression outerWhr = mySelect.getOuterWhere();
					if (outerWhr != null) {
						if (evalWhr.evalWhere(outerWhr, tuple, projMap, dataTypeMap)) {
							isValidTuple = true;
						}
						else {
							isValidTuple = false;
						}
					}
					else {
						isValidTuple = true;
					}
				}
				//System.out.println(isValidTuple);
				if ((isValidTuple) && (groupByElem != null)) {
					//System.out.println(tuple);
					PrimitiveValue[] whrRes = evalWhr.getPrimArr(tuple, dataTypeMap, columnDefinitions);
					
					//System.out.println(whrRes[0]);
					grpBy.evalGroupBy(whrRes);
				}
				
				else if ((isValidTuple) && ((groupByElem == null) && (orderByElem != null))) {
					PrimitiveValue[] whrRes = evalWhr.getPrimArr(tuple, dataTypeMap, columnDefinitions);
					ordBy.evalOrderBy(whrRes);
				}
				
				else if (isValidTuple) {
					primValues = pt.printTuples(outerProj, tuple, colNameMap, dataTypeMap, primValues);
				}
			}
    	}
		currSelect = new String[columnDefinitions.size()];
		for (int i = 0; i < columnDefinitions.size(); i++) {
			currSelect[i] = columnDefinitions.get(i).getColumnName();
		}
		
		if (groupByElem != null) {
			grpBy.pqFlush();	
			grpBy.mergeFiles(currSelect, dataTypeMap, columnDefinitions);
			grpBy.applyAggregate(outerProj, projMap, dataTypeMap, groupByElem, rowCnt);
		} 
		else if (orderByElem != null){
			ordBy.pqFlush();
			ordBy.mergeFiles(currSelect, dataTypeMap, columnDefinitions, outerProj, colNameMap, rowCnt);
		}
		else {
			if (primValues.length > 0)
				new PrintTable().printAggregate(primValues, outerProj);
		}
		br.close();
	}
	public static void main(String[] args) throws JSQLParserException, IOException, ParseException, InvalidPrimitive, SQLException {
		// TODO Auto-generated method stub
		
		// TODO Auto-generated method stub
		// Code to structure the schema details from CREATE
		
		System.out.print("$>");
		// Read query from console
		BufferedReader br = null;
		StringBuilder fullQuery = new StringBuilder("");
		br = new BufferedReader(new InputStreamReader(System.in));
		String currLine;
		//System.out.println(args[args.length - 1]);
		
		while ((currLine = br.readLine()) != null) {
			if (currLine.indexOf(';') == -1) {
				fullQuery.append(currLine+" ");
				continue;
			}
			if (fullQuery.length() > 0) {
				fullQuery.append(currLine);
				currLine = fullQuery.toString();
			}
			currLine = currLine.trim();
			if (currLine.startsWith("SELECT")) {
				currLine = currLine.replace(tableName+".", "");
				if (currLine.indexOf('.') != -1){
					String[] currArr = currLine.split("[A-Z]+[.]");
					currLine = "";
					for (int i = 0; i < currArr.length; i++) {
						currLine += currArr[i].substring(0, currArr[i].length() -1)+" ";
					}
				}
			}
			String inputQuery = currLine; 
			CCJSqlParser parser = new CCJSqlParser(new StringReader(inputQuery));
			Statement query = parser.Statement();
			
			if (args[args.length - 1].equals("--in-mem")) {
				isInMem = true;
				//System.out.println("RCHD");
			}
			if (query instanceof CreateTable) {
				MyCreateTable myTable = new MyCreateTable(query);
				CreateTable table = myTable.getTable();
				tableName = myTable.getTableName();
				colMap = myTable.getSchemaDetails(table);
				dataTypeMap = myTable.getDatatypes();
				columnDefinitions = myTable.getTable().getColumnDefinitions();
				indexes = myTable.getIndexes();
				if (isInMem) {
					csvList = myTable.getCsvList(tableName+".csv");
				}
				//List<TreeMap<PrimitiveValue, Integer>> indexList = myTable.getIndexMap(indexes);
//				TreeMap<PrimitiveValue, Integer> x = indexList.get(0);
//				for (PrimitiveValue key : x.keySet()) {
//					System.out.println(key + " --- "+x.get(key));
//				}
//				
//				RandomAccessFile raf = new RandomAccessFile("SHIPDATEindex/sorted.csv", "rw");
//				
//				for (PrimitiveValue key : x.keySet()) {
//					 raf.seek(x.get(key));
//					 System.out.println(raf.readLine());
//					 System.out.println();
//					 System.out.println();
//				}
//				raf.close();
			}
			
			else if(query instanceof Select){
				MySelect mySelect = new MySelect(query);
			    Select select = (Select)query;
			    SelectBody selectBody = select.getSelectBody();
			    if (selectBody != null) {
			    	if (selectBody instanceof PlainSelect) {
			    		
			    		// Get the TABLE name
			    		StringBuilder sb = new StringBuilder(tableName);
			    		sb.append(".csv");
			    		
			    		groupByElem = mySelect.getGroupByElements();
			    		orderByElem = mySelect.getOrderByElements();
			    		
			    		limit = mySelect.getLimit();
			    		//System.out.println(limit);
			    		printTableQueris(sb.toString(), colMap, dataTypeMap, mySelect);						
			    	}
			    	
			    	else {
			    		throw new JSQLParserException("No query found"+query);
			    	}
			    }
			    else {
			    	throw new JSQLParserException("No query found"+query);
			    }
			    
			    /** Do something with the select operator **/
			    
			    
			  } else {
				  	throw new JSQLParserException("I can't understand "+query);
			  }
			System.out.print("$>");
			fullQuery = new StringBuilder("");
			System.gc();
		}
	
	}

}
