package dubstep;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import net.sf.jsqlparser.eval.Eval;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.PrimitiveValue;
import net.sf.jsqlparser.expression.PrimitiveValue.InvalidPrimitive;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

public class PrintTable {
	private PrimitiveValue currVal;
	private int rowCnt = 0;
	static int count = 0;
	
	public PrintTable() {
		
	}
	
	public PrintTable(int rowCount) {
		this.rowCnt = rowCount;
	}
	
	public PrimitiveValue[] printTuples(List<SelectItem> selectCols, String tuple, Map<String, Integer> colNameMap, Map<String, String> dataTypeMap, PrimitiveValue[] primValues) throws InvalidPrimitive, SQLException {
		int index = 0;
		int pIndex = 0;
		count++;
		for (SelectItem col : selectCols) {
			if (!(col instanceof AllColumns)) {
				SelectExpressionItem item = (SelectExpressionItem) col;
				Expression expression = item.getExpression();
				Eval eval = new Eval() {
					
					@Override
					public PrimitiveValue eval(Column arg0) throws SQLException {
						// TODO Auto-generated method stub
						//String[] tupleList = tuple.split(Pattern.quote("|"));
						StringTokenizer st = new StringTokenizer(tuple, "\\|");
						String[] tupleList = new String[colNameMap.size()];
						for (int i = 0; i < tupleList.length; i++) {
							tupleList[i] = st.nextToken();
						}
						int index = colNameMap.get(arg0.toString());
						String dataType = dataTypeMap.get(arg0.toString());
						
						if(dataType.equalsIgnoreCase("int")){
							return new LongValue(Long.parseLong(tupleList[index]));
						}
						
						if(dataType.equalsIgnoreCase("decimal")){
							return new DoubleValue(Double.parseDouble(tupleList[index]));
						}
						
						if(dataType.equalsIgnoreCase("date")){
							return new DateValue(tupleList[index]);
						}
						
						if (dataType.equalsIgnoreCase("String") || dataType.equalsIgnoreCase("varchar") || dataType.equalsIgnoreCase("char")){
							return new StringValue(tupleList[index]);
						}
						
						return null;
					}
				};
				
				if (expression instanceof Function) {
					
					Function function = (Function) expression;
					String funName = function.getName();
					ExpressionList ex = function.getParameters();
					List<Expression> list = new ArrayList<Expression>();
					if (ex != null)
						list = ex.getExpressions();
					
					switch(funName) {
					
						case "MIN": currVal = eval.eval(list.get(0));
									if (primValues[pIndex] == null) {
										primValues[pIndex] = currVal;
									}
									else {
										if (eval.eval(new MinorThan(currVal, primValues[pIndex])).toBool()) {
											primValues[pIndex] = currVal;
										}
									}break;
									
						case "SUM" : if (primValues[pIndex] == null) {
											primValues[pIndex] = new LongValue(0);
									 }
									 PrimitiveValue currSum = eval.eval(list.get(0));
									 if (currSum != null)
										 primValues[pIndex] = eval.eval(new Addition(primValues[pIndex], currSum));
									 break;
									
						case "COUNT" : if (primValues[pIndex] == null) {
											primValues[pIndex] = new LongValue(0);
									   }
									   primValues[pIndex] = eval.eval(new Addition(primValues[pIndex], new LongValue(1)));
									   break;
									   
						case "MAX" : currVal = eval.eval(list.get(0));
									 if (primValues[pIndex] == null) {
										primValues[pIndex] = currVal;
									 }
									 else {				
										if (eval.eval(new GreaterThan(currVal, primValues[pIndex])).toBool()) {
											primValues[pIndex] = currVal;
										}
									 }
									 break;
									
						case "AVG" : if (primValues[pIndex] == null) {
										primValues[pIndex] = new DoubleValue(0);
									 }
									 PrimitiveValue currVal = eval.eval(list.get(0));
									 if (currVal != null)
										 primValues[pIndex] = eval.eval(new Addition(primValues[pIndex], currVal));
									 break;
						
						default : System.out.print("Invalid function");break;
					}
				}
				else {
					if (rowCnt > 0) {
						try {
							PrimitiveValue primVal = eval.eval(expression);
							if (primVal != null) {
								if (index == (selectCols.size() - 1)) {
									System.out.println(primVal.toString());
									rowCnt--;
								}
								else
									System.out.print(primVal.toString()+"|");
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			else {
				if (rowCnt > 0) {
					System.out.println(tuple);
					rowCnt--;
				}
			}
			index++;
			pIndex++;
		}
		return primValues;
	}
	
	public void printAggregate(PrimitiveValue[] primValues, List<SelectItem> outerProj) throws SQLException {
        boolean skipPrint = false;
        Eval eval = new Eval() {
            @Override
            public PrimitiveValue eval(Column arg0) throws SQLException {
                // TODO Auto-generated method stub
                return null;
            }
        };
        for (int i = 0; i < outerProj.size(); i++) {
            if(outerProj.get(i).toString().startsWith("AVG") && primValues[i] != null) {
                PrimitiveValue cnt = new LongValue(count);
                primValues[i] =  eval.eval(new Division(primValues[i], cnt));
            }
        }
        int index = 0;
        for (PrimitiveValue pv : primValues) {
            //HD Code
            //System.out.println("inside for with pv = " + pv);
            if(pv == null)
            {
                SelectItem si = outerProj.get(index);
                SelectExpressionItem item = (SelectExpressionItem) si;
                Expression expression = item.getExpression();
                if (expression instanceof Function) {
                    Function function = (Function) expression;
                    String funName = function.getName();
                    if(index == 0)
                    {
                        if(funName.equals("COUNT"))
                        {
                            System.out.print("0");
                        }
                        
                        else
                        {
                            System.out.print("");
                        }
                    }
                    
                    else
                    {
                        if(funName.equals("COUNT"))
                        {
                            System.out.print("|0");
                        }
                        
                        else
                        {
                            System.out.print("|");
                        }
                    }
                    skipPrint = false;
                }
                
                else
                {
                    skipPrint = true;
                }
                index++;
            }
            //end of HD code remember to remove following else if deleting
            else
            {
                if (pv != null) {
                    if (index == 0) {
                        System.out.print(pv.toString());
                        index++;
                    }
                    else {
                        System.out.print("|"+pv.toString());
                    }
                }
            }
        }
        if(!skipPrint)
        {
            System.out.println();
        }

    }
}
