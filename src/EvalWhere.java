package dubstep;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import net.sf.jsqlparser.eval.Eval;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.PrimitiveValue;
import net.sf.jsqlparser.expression.PrimitiveValue.InvalidPrimitive;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

public class EvalWhere {
	static PrimitiveValue currVal;
	//PrimitiveValue[] output;
	static int count = 0;
	public EvalWhere() {
		
	}
	
	public PrimitiveValue[] getPrimArr(String tuple, Map<String, String> dataTypeMap, List<ColumnDefinition> colDef) {
		StringTokenizer st = new StringTokenizer(tuple, "\\|");
		String[] tupleList = new String[colDef.size()];
		for (int i = 0; i < tupleList.length; i++) {
			tupleList[i] = st.nextToken();
		}
		List<String> colNames = new ArrayList<String>();
		PrimitiveValue[] op = new PrimitiveValue[colDef.size()];
		
		for (ColumnDefinition cd : colDef) {
			colNames.add(cd.getColumnName());
		}
		for (int i = 0; i < colNames.size(); i++) {
			String dataType = dataTypeMap.get(colNames.get(i));
			if(dataType.equalsIgnoreCase("int")){
				op[i] = new LongValue(Long.parseLong(tupleList[i]));
			}
			
			else if(dataType.equalsIgnoreCase("decimal")){
				op[i] = new DoubleValue(Double.parseDouble(tupleList[i]));
			}
			
			else if(dataType.equalsIgnoreCase("date")){
				//System.out.println(tupleList[i]);
				op[i] = new DateValue(tupleList[i]);
			}
			
			else if (dataType.equalsIgnoreCase("String") || dataType.equalsIgnoreCase("varchar") || dataType.equalsIgnoreCase("char")) {
				op[i] = new StringValue(tupleList[i]);
			}
		}
		return op;
	}
	
	
	public String evalExpression(List<SelectItem> currSelect, String tuple, Map<String, Integer> colNameMap,
			Map<String, String> dataTypeMap) throws SQLException {
		
		StringTokenizer st = new StringTokenizer(tuple, "\\|");
		String[] tupleList = new String[colNameMap.size()];
		for (int i = 0; i < tupleList.length; i++) {
			tupleList[i] = st.nextToken();
		}
		
		Eval eval = new Eval(){
			@Override
			public PrimitiveValue eval(Column arg0) throws SQLException {
				// TODO Auto-generated method stub
				String col = arg0.toString();
				int index = colNameMap.get(col);
				String dataType = dataTypeMap.get(col);
				
				if(dataType.equalsIgnoreCase("int")){
					return new LongValue(Long.parseLong(tupleList[index]));
				}
				
				else if(dataType.equalsIgnoreCase("decimal")){
					return new DoubleValue(Double.parseDouble(tupleList[index]));
				}
				
				else if(dataType.equalsIgnoreCase("date")){
					return new DateValue(tupleList[index]);
				}
				
				else if (dataType.equalsIgnoreCase("String") || dataType.equalsIgnoreCase("varchar") || dataType.equalsIgnoreCase("char")) {
					return new StringValue(tupleList[index]);
				}
				
				return null;
			}
		};
		
		String newTuple = "";
		int index  = 0;
		for (SelectItem si : currSelect) {
			SelectExpressionItem item = (SelectExpressionItem) si;
			Expression expression = item.getExpression();
			PrimitiveValue primVal = eval.eval(expression);
			if (primVal != null) {
				if (index == currSelect.size() - 1)
					newTuple += primVal.toRawString();
				else
					newTuple += primVal.toRawString()+"|";
			}
			index++;
		}
		return newTuple;
	}

	public boolean evalWhere(Expression whereItem,
			String tuple, Map<String, Integer> colNameMap,
			Map<String, String> dataTypeMap) throws InvalidPrimitive, SQLException {
		// TODO Auto-generated method stub
		
		StringTokenizer st = new StringTokenizer(tuple, "\\|");
		String[] tupleList = new String[colNameMap.size()];
		for (int i = 0; i < tupleList.length; i++) {
			tupleList[i] = st.nextToken();
		}
		
		Eval eval = new Eval(){
			@Override
			public PrimitiveValue eval(Column arg0) throws SQLException {
				// TODO Auto-generated method stub
				
				String col = arg0.toString();		
			//System.out.println(col);
//			System.out.println(colNameMap.size());
//				for (String key : colNameMap.keySet()) {
//					System.out.println(key +" -- "+colNameMap.get(key));
//				}
				int index = colNameMap.get(col);
				String dataType = dataTypeMap.get(col);
				
				if(dataType.equalsIgnoreCase("int")){
					return new LongValue(Long.parseLong(tupleList[index]));
				}
				
				else if(dataType.equalsIgnoreCase("decimal")){
					return new DoubleValue(Double.parseDouble(tupleList[index]));
				}
				
				else if(dataType.equalsIgnoreCase("date")){
					return new DateValue(tupleList[index]);
				}
				
				else if (dataType.equalsIgnoreCase("String") || dataType.equalsIgnoreCase("varchar") || dataType.equalsIgnoreCase("char")) {
					return new StringValue(tupleList[index]);
				}
				
				return null;
			}
		};
		//System.out.println(whereItem);
		return (eval.eval(whereItem)).toBool();
	}
	
	public PrimitiveValue[] getPrimRes(String tuple, String[] currSelect, Map<String, String> dataTypeMap) {
		StringTokenizer st = new StringTokenizer(tuple, "\\|");
		String[] tupleList = new String[currSelect.length];
		for (int i = 0; i < tupleList.length; i++) {
			tupleList[i] = st.nextToken();
		}
		PrimitiveValue[] resArr = new PrimitiveValue[currSelect.length];
		for (int i = 0; i < currSelect.length; i++) {
//			System.out.println(dataTypeMap.size());
//			System.out.println(currSelect[i]);
			String dType = dataTypeMap.get(currSelect[i]);
			
			if(dType.equalsIgnoreCase("int")){
				resArr[i] = new LongValue(Long.parseLong(tupleList[i]));
			}
			
			else if(dType.equalsIgnoreCase("decimal")){
				resArr[i] = new DoubleValue(Double.parseDouble(tupleList[i]));
			}
			
			else if(dType.equalsIgnoreCase("date")){
				resArr[i] = new DateValue(tupleList[i]);
			}
			
			else if (dType.equalsIgnoreCase("String") || dType.equalsIgnoreCase("varchar") || dType.equalsIgnoreCase("char")) {
				resArr[i] = new StringValue(tupleList[i]);
			}
		}
		return resArr;
	}

	public PrimitiveValue[] getOuterSelectAggr(String tuple, PrimitiveValue[] primArr, List<SelectItem> proj, Map<String, Integer> colNameMap, Map<String, String> dataTypeMap) throws SQLException {
		// TODO Auto-generated method stub
		int pIndex = 0;
		count++;
		for (SelectItem si : proj) {
			SelectExpressionItem item = (SelectExpressionItem) si;
			Expression expression = item.getExpression();
			
			Eval eval = new Eval() {
				@Override
				public PrimitiveValue eval(Column arg0) throws SQLException {
					// TODO Auto-generated method stub
					//String[] tupleList = tuple.split(Pattern.quote("|"));
					StringTokenizer st = new StringTokenizer(tuple, "\\|");
					String[] tupleList = new String[colNameMap.size()];
					for (int i = 0; i < tupleList.length; i++) {
						tupleList[i] = st.nextToken();
					}
					int index = colNameMap.get(arg0.toString());
//					System.out.println("ARG 0 "+arg0.toString());
//					for (String key : colNameMap.keySet()) {
//						System.out.print(key +" ");
//					}
//					System.out.println(dataTypeMap == null);
//					for (String key : dataTypeMap.keySet()) {
//						System.out.print(key +" ");
//					}
					
					String dataType = dataTypeMap.get(arg0.toString());
					
					if(dataType.equalsIgnoreCase("int")){
						return new LongValue(Long.parseLong(tupleList[index]));
					}
					
					if(dataType.equalsIgnoreCase("decimal")){
						return new DoubleValue(Double.parseDouble(tupleList[index]));
					}
					
					if(dataType.equalsIgnoreCase("date")){
						return new DateValue(tupleList[index]);
					}
					
					if (dataType.equalsIgnoreCase("String") || dataType.equalsIgnoreCase("varchar") || dataType.equalsIgnoreCase("char")){
						return new StringValue(tupleList[index]);
					}
					
					return null;
				}
			};
		
		
			if (expression instanceof Function) {
				
				Function function = (Function) expression;
				String funName = function.getName();
				ExpressionList ex = function.getParameters();
				List<Expression> list = new ArrayList<Expression>();
				if (ex != null)
					list = ex.getExpressions();
				//System.out.println(funName);
				switch(funName) {
				
					case "MIN": currVal = eval.eval(list.get(0));
								if (primArr[pIndex] == null) {
									primArr[pIndex] = currVal;
								}
								else {
									if (eval.eval(new MinorThan(currVal, primArr[pIndex])).toBool()) {
										primArr[pIndex] = currVal;
									}
								}break;
								
					case "SUM" : if (primArr[pIndex] == null) {
									primArr[pIndex] = new LongValue(0);
								 }
								 PrimitiveValue currSum = eval.eval(list.get(0));
								 if (currSum != null)
									 primArr[pIndex] = eval.eval(new Addition(primArr[pIndex], currSum));
								 break;
								
					case "COUNT" : if (primArr[pIndex] == null) {
										primArr[pIndex] = new LongValue(0);
								   }
									primArr[pIndex] = eval.eval(new Addition(primArr[pIndex], new LongValue(1)));
								   break;
								   
					case "MAX" : currVal = eval.eval(list.get(0));
								 if (primArr[pIndex] == null) {
									 primArr[pIndex] = currVal;
								 }
								 else {				
									if (eval.eval(new GreaterThan(currVal, primArr[pIndex])).toBool()) {
										primArr[pIndex] = currVal;
									}
								 }
								 break;
								 
					case "AVG" : if (primArr[pIndex] == null) {
									primArr[pIndex] = new DoubleValue(0);
								 }
								 PrimitiveValue currVal = eval.eval(list.get(0));
								 if (currVal != null)
									 primArr[pIndex] = eval.eval(new Addition(primArr[pIndex], currVal));
								 break;
								
					default : System.out.print("Invalid function");break;
				}
			}
			else {
				try {
					PrimitiveValue primVal = eval.eval(expression);
					primArr[pIndex] = primVal;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			pIndex++;
		}
	return primArr;
	}
	
	public PrimitiveValue[] evalAvg(PrimitiveValue[] arr, List<SelectItem> proj, int grpCnt) throws SQLException {
		Eval eval = new Eval() {
			@Override
			public PrimitiveValue eval(Column arg0) throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		for (int i = 0; i < proj.size(); i++) {
			if (proj.get(i).toString().startsWith("AVG") && arr[i] != null) {
				PrimitiveValue cnt = new LongValue(grpCnt);
				arr[i] = eval.eval(new Division(arr[i], cnt));
			}
		}
		return arr;
	}
	
}
